package week4.day2;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import excel.ReadExcel;
import week4_wrappers.ProjectMethods;

public class TC001_smoke extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDesc = "Create A new Lead";
		author = "Karthikeyan";
		category = "smoke";
	}
	@Test(dataProvider = "test")
	public void test(String cname, String fname, String lname, String Email, String Phone) {
		click(locateElement("linktext","Create Lead"));
		type(locateElement("createLeadForm_companyName"),cname);
		type(locateElement("createLeadForm_firstName"), fname);
		type(locateElement("createLeadForm_lastName"), lname);
		click(locateElement("class","smallSubmit"));
		
	}
	@DataProvider(name = "test", indices = {1})
		public Object[][] fetchData() throws IOException{
		Object[][] data = ReadExcel.readExcel();
		return data;		
		
		
		/*String[][] data = new String[2][3];
		data[0][0] = "Servion";
		data[0][1] = "Karthikeyan";
		data[0][2] = "Sridar";
		
		data[1][0] = "IBM";
		data[1][1] = "Test";
		data[1][2] = "leaf";
		
		return data;*/	
	}
}
